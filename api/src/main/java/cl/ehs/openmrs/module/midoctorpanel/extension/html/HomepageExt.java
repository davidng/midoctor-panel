package cl.ehs.openmrs.module.midoctorpanel.extension.html;

import org.openmrs.module.web.extension.PortletExt;

/**
 * Provides the MiDoctor Homepage
 */
public class HomepageExt extends PortletExt {
	
	/**
	 * @see PortletExt#getPortletUrl()
	 */
	@Override
	public String getPortletUrl() {
		return "midoctorHome";
	}
	
	/**
	 * @see PortletExt#getPortletParameters()
	 */
	@Override
	public String getPortletParameters() {
		return "";
	}
}
