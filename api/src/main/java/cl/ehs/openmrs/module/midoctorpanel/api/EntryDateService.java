package cl.ehs.openmrs.module.midoctorpanel.api;

import java.util.List;

import org.openmrs.Patient;
import org.openmrs.User;
import org.openmrs.api.OpenmrsService;

import cl.ehs.openmrs.module.midoctorpanel.db.EntryDateDAO;
import cl.ehs.openmrs.module.midoctorpanel.model.EntryDate;

import org.openmrs.cohort.CohortDefinitionItemHolder;

public interface EntryDateService extends OpenmrsService {
	
	public void setEntryDateDAO(EntryDateDAO dao);
	
	public void clearEntryDateTable();
	
	public void saveEntryDate(EntryDate date);
	
	public EntryDate findByPatientAndCohort(int pId, int cId);
	
	//public void updateEntryDateForCohort(CohortDefinitionItemHolder c, Date);
	
	public List<Patient> getPatients(String query, Integer start, Integer length);
	
	public Integer getCountOfPatients(String query);
	
	public List<Integer> getAssignedHealthCenters(User user);
	
	public List<Integer> getHealthCenters(Patient patient);
}
