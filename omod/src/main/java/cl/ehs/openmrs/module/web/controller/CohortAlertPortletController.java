package cl.ehs.openmrs.module.web.controller;

import cl.ehs.openmrs.module.midoctorpanel.api.EntryDateService;
import cl.ehs.openmrs.module.midoctorpanel.cohortalert.CohortAlertService;
import cl.ehs.openmrs.module.midoctorpanel.cohortalert.CohortAlertCompatibilityImpl;
import cl.ehs.openmrs.module.midoctorpanel.model.EntryDate;
import cl.ehs.openmrs.module.web.model.CohortAlertRow;
import cl.ehs.openmrs.module.web.model.CohortUnit;

import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

import org.openmrs.Patient;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.cohort.CohortDefinitionItemHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

/**
 * Controller for midoctorpanel's cohortAlert portlet.
 */
@Controller("midoctorpanel.CohortAlertPortletController")
@RequestMapping("/module/midoctorpanel/portlets/cohortAlert")
public class CohortAlertPortletController {
	
	private EntryDateService service;
	
	private void getModuleService() {
		if (service == null)
			service = (EntryDateService) Context.getService(EntryDateService.class);
	}
	
	private boolean isPatientViewable(Patient patient, User user) {
		getModuleService();
		List<Integer> patientHealthCenter = service.getHealthCenters(patient);
		List<Integer> assingedHealthCenter = service.getAssignedHealthCenters(user);
		for (Integer locationId : patientHealthCenter) {
			if (locationId == null)
				continue;
			if (assingedHealthCenter.contains(locationId))
				return true;
		}
		return false;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public void showForm() {
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String handleSubmission(Errors errors, WebRequest request) {
		return "redirect:index.htm";
	}
	
	/**
	 * Returns the list of the cohortUnit for all the patients inside the patientSearch cohorts.
	 * 
	 * @return List<CohortUnit>
	 */
	@ModelAttribute("cohortAlertModel")
	public List<CohortUnit> getModel() throws ParseException {
		getModuleService();
		User user = Context.getAuthenticatedUser();
		
		List<CohortUnit> cohortUnit = new LinkedList<CohortUnit>();
		CohortAlertService cas = new CohortAlertCompatibilityImpl();
		List<CohortDefinitionItemHolder> cohortDefs = cas.getCohortDefs();
		
		for (CohortDefinitionItemHolder c : cohortDefs) {
			CohortUnit unit = new CohortUnit();
			unit.setCohortName(c.getName());
			unit.setCohortId(Integer.parseInt(c.getKey().split(":")[0].trim()));
			List<CohortAlertRow> rows = new LinkedList<CohortAlertRow>();
			
			//EntryDateService service = Context.getService(EntryDateService.class);
			
			for (Patient p : cas.getPatientsInCohort(c)) {
				if (!isPatientViewable(p, user))
					continue;
				
				CohortAlertRow row = new CohortAlertRow();
				
				row.setPatient(p);
				EntryDate entry = service.findByPatientAndCohort(p.getId(), unit.getCohortId());
				
				if (entry != null) {
					row.setCohortEntryDate(entry.getEntryDate());
					row.setStatus(cas.lateStatusCheck(entry.getDaysSinceEntry()));
				} else {
					
					row.setCohortEntryDate(new java.util.Date());
					row.setStatus(cas.lateStatusCheck(0));
				}
				rows.add(row);
			}
			unit.setRows(rows);
			cohortUnit.add(unit);
		}
		return cohortUnit;
	}
}
