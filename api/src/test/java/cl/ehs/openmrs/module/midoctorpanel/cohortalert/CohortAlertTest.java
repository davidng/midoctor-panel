package cl.ehs.openmrs.module.midoctorpanel.cohortalert;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.openmrs.Cohort;
import org.openmrs.Patient;
import org.openmrs.api.CohortService;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.cohort.CohortDefinition;
import org.openmrs.cohort.CohortDefinitionItemHolder;
import org.openmrs.report.EvaluationContext;
import org.openmrs.report.Parameter;
import org.openmrs.reporting.PatientSearch;
import org.openmrs.reporting.SearchArgument;

/**
 * Testing with real direct database instead of mocking
 */
public class CohortAlertTest {
	
	public CohortAlertTest() {
	}
	
	@Before
	public void setUp() throws Exception {
	}
	
	@After
	public void tearDown() {
	}
	
	@Test
	@Ignore
	public void testGetCohortAlert() throws ParseException {
		CohortAlertService ca = new CohortAlertCompatibilityImpl();
		List<CohortDefinitionItemHolder> cohortDefs = ca.getCohortDefs();
		for (CohortDefinitionItemHolder c : cohortDefs) {
			List<Patient> patientsInCohort = ca.getPatientsInCohort(c);
			for (Patient p : patientsInCohort) {
				Date patientEntryDateIntoCohort = ca.getPatientEntryDateIntoCohort(c, p);
			}
		}
	}
	
	/**
	 * Test of getCohortByName method, of class CohortAlert.
	 */
	@Test
	@Ignore
	public void testGetCohortByName() {
		CohortService cohortService = Context.getCohortService();
		List<CohortDefinitionItemHolder> allCohortDefinitions = cohortService.getAllCohortDefinitions();
		for (CohortDefinitionItemHolder c : allCohortDefinitions) {
			if (c.getKey().contains("org.openmrs.reporting.PatientSearch")) {
				Cohort cohort1 = cohortService.evaluate(c.getCohortDefinition(), null);
				PatientSearch ps = (PatientSearch) c.getCohortDefinition();
				List<SearchArgument> arguments = ps.getArguments();
				for (SearchArgument sa : arguments) {
					System.out.println("sa ===> " + sa.getName() + " VAL ==>" + sa.getValue() + " CLASS ==>"
					        + sa.getPropertyClass().getName());
				}
				EvaluationContext ec = new EvaluationContext();
				ec.setBaseCohort(cohort1);
				ec.addParameterValue(new Parameter("sinceDate", "Since Date", Date.class, null), new Date());
				Cohort cohort2 = cohortService.evaluate(c.getCohortDefinition(), ec);
				Cohort cs = Cohort.intersect(cohort1, cohort2);
				
				CohortDefinition cohortDefinition = c.getCohortDefinition();
				Set<Integer> patientIds = cohort1.getPatientIds();
				PatientService patientService = Context.getPatientService();
				for (Integer i : patientIds) {
					Patient patient = patientService.getPatient(i);
				}
			}
		}
	}
}
