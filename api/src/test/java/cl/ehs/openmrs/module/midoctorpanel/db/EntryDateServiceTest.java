package cl.ehs.openmrs.module.midoctorpanel.db;

import cl.ehs.openmrs.module.midoctorpanel.api.EntryDateService;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openmrs.api.context.Context;

import java.text.ParseException;

/**
 * Created with IntelliJ IDEA. User: tamvu Date: 18/11/13 Time: 9:18 AM To change this template use
 * File | Settings | File Templates.
 */
public class EntryDateServiceTest {
	
	@Before
	public void setUp() throws Exception {
	}
	
	@After
	public void tearDown() {
	}
	
	@Test
	@Ignore
	public void testClearEntryDateTable() {
		Context.openSession();
		EntryDateService service = Context.getService(EntryDateService.class);
		service.clearEntryDateTable();
		
	}
}
