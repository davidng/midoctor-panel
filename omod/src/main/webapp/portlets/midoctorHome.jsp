<%@ include file="/WEB-INF/template/include.jsp" %>
<c:set var="DO_NOT_INCLUDE_JQUERY" value="true"/>
<%@ include file="/WEB-INF/template/header.jsp"%>
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/jquery-1.8.3.min.js" />
<script>
    $j = jQuery.noConflict();
</script>
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/jquery-ui-1.9.2.custom.min.js" />
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/styles/jquery-ui-1.9.2.custom.min.css" />
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/styles/layout-default-latest.css" />
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/jquery.layout-latest.min.js" />

<script>
    $j(document).ready(function() {
        $j('div.splitContainer').css('height', ($j('body').height() - $j('#banner').height() - $j('#footer').height() - 20));
        $j('div.splitContainer').layout({
            west__size: 700,
            stateManagement__enabled: true
        });
        if ($j.browser.mozilla == true) {
            $j('div.ui-layout-resizer').css('left', Number($j('div.ui-layout-resizer').css('left').replace('px', '')) - 2);
        }
    });
</script>
<div class="splitContainer">
    <div class="ui-layout-west"> 
        <openmrs:require privilege="View Patients" otherwise="/login.htm" redirect="/findPatient.htm" />	
        <openmrs:portlet id="findPatient" url="../module/midoctorpanel/portlets/findPatient" parameters="size=full|postURL=${pageContext.request.contextPath}/patientDashboard.form|hideAddNewPatient=true|showIncludeVoided=false" /> 
    </div>
    <div class="ui-layout-center" style="background-color:#<openmrs:globalProperty key="midoctorpanel.cohortAlertPortletBgColor"/>"> 
        <openmrs:portlet id="cohortAlert" url="../module/midoctorpanel/portlets/cohortAlert" />  
    </div>
</div>
<%@ include file="/WEB-INF/template/footer.jsp"%>