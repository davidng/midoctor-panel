package cl.ehs.openmrs.module.web.model;

import java.util.List;

public class CohortUnit {
	
	private String cohortName;
	
	private List<CohortAlertRow> rows;
	
	private Integer cohortId;
	
	public Integer getCohortId() {
		return cohortId;
	}
	
	public void setCohortId(Integer cohortId) {
		this.cohortId = cohortId;
	}
	
	public String getCohortName() {
		return cohortName;
	}
	
	public void setCohortName(String cohortName) {
		this.cohortName = cohortName;
	}
	
	public List<CohortAlertRow> getRows() {
		return rows;
	}
	
	public void setRows(List<CohortAlertRow> rows) {
		this.rows = rows;
	}
	
}
