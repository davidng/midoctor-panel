package cl.ehs.openmrs.module.midoctorpanel.api.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import cl.ehs.openmrs.module.midoctorpanel.api.EntryDateService;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifierType;
import org.openmrs.User;
import org.openmrs.api.APIException;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.util.OpenmrsUtil;

import cl.ehs.openmrs.module.midoctorpanel.db.EntryDateDAO;
import cl.ehs.openmrs.module.midoctorpanel.model.EntryDate;

public class EntryDateServiceImpl extends BaseOpenmrsService implements EntryDateService {
	
	protected final Log log = LogFactory.getLog(EntryDateServiceImpl.class);
	
	private EntryDateDAO dao;
	
	@Override
	public void setEntryDateDAO(EntryDateDAO dao) {
		this.dao = dao;
	}
	
	@Override
	public void clearEntryDateTable() {
		
		dao.deleteAll();
		
	}
	
	@Override
	public void saveEntryDate(EntryDate date) {
		dao.setEntryDate(date);
	}
	
	@Override
	public EntryDate findByPatientAndCohort(int pId, int cId) {
		return dao.findEntryDate(pId, cId);
	}
	
	public List<Patient> getPatients(String query, Integer start, Integer length) throws APIException {
		List<Patient> patients = new Vector<Patient>();
		if (StringUtils.isBlank(query))
			return patients;
		
		// if there is a number in the query string
		if (query.matches(".*\\d+.*")) {
			log.debug("[Identifier search] Query 2: " + query);
			//System.out.println("[Identifier search] Query: " + query);
			return getPatients(null, query, null, false, start, length);
		} else {
			// there is no number in the string, search on namLe
			return getPatients(query, null, null, false, start, length);
		}
	}
	
	public List<Patient> getPatients(String name, String identifier, List<PatientIdentifierType> identifierTypes,
	        boolean matchIdentifierExactly, Integer start, Integer length) throws APIException {
		if (identifierTypes == null)
			identifierTypes = Collections.emptyList();
		
		return dao.getPatients(name, identifier, identifierTypes, matchIdentifierExactly, start, length);
	}
	
	public Integer getCountOfPatients(String query) {
		int count = 0;
		if (StringUtils.isBlank(query))
			return count;
		List<PatientIdentifierType> emptyList = new Vector<PatientIdentifierType>();
		// if there is a number in the query string
		if (query.matches(".*\\d+.*")) {
			log.debug("[Identifier search] Query: " + query);
			//System.out.println("[Identifier search] Query 1: " + query);
			return OpenmrsUtil.convertToInteger(dao.getCountOfPatients(null, query, emptyList, false));
		} else {
			// there is no number in the string, search on name
			return OpenmrsUtil.convertToInteger(dao.getCountOfPatients(query, null, emptyList, false));
		}
	}
	
	@Override
	public List<Integer> getAssignedHealthCenters(User user) {
		return dao.getAssignedHealthCenters(user);
	}
	
	@Override
	public List<Integer> getHealthCenters(Patient patient) {
		return dao.getHealthCenters(patient);
	}
}
