package cl.ehs.openmrs.module.midoctorpanel.cohortalert;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import org.openmrs.Patient;
import org.openmrs.api.PatientIdentifierException;
import org.openmrs.cohort.CohortDefinitionItemHolder;

/**
 * Interface to define functionality for Cohort Alert Implementations are going to cater to the
 * style of Cohorts or Modules that provide Cohorts
 */
public interface CohortAlertService {
	
	public List<CohortDefinitionItemHolder> getCohortDefs();
	
	public List<Patient> getPatientsInCohort(CohortDefinitionItemHolder c) throws PatientIdentifierException;
	
	public Date getPatientEntryDateIntoCohort(CohortDefinitionItemHolder c, Patient patient) throws ParseException;
	
	public CohortAlertStatus lateStatusCheck(Date date);
	
	public CohortAlertStatus lateStatusCheck(int dateDiff);
	
}
