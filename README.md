MiDoctor Panel Module
---------------
An OpenMRS module that creates an alerts panel in the Find/Create patient page which allows users to more easily see the patients they need to assist. The module overrides the default OpenMRS homepage and adds 2 portlets to it:

  - Patient Search portlet (built-in OpenMRS)
  - Cohort Alert Portlet (a flexible Cohort Results viewer)

The Cohort Alert Portlet allows admin to customize the cohorts that need to be displayed and the patients inside the cohort. Thus the cohort members are patients who fall under the pre-defined Cohort Definition. You can build CohortDefinitions using the ReportingCompatibility module or the Reporting module.


**The module has been developed by eHS Chile and more documentation can be found at the [MiDoctor Homepage]**

  [MiDoctor Homepage]: http://wiki.ehs.cl:8090/display/DESARROLLO/Requirement+for+alerts+panel+in+MiDoctor+Homepage