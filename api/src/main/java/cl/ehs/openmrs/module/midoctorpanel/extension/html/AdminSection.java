package cl.ehs.openmrs.module.midoctorpanel.extension.html;

import java.util.HashMap;
import java.util.Map;
import org.openmrs.module.Extension;
import org.openmrs.module.web.extension.AdministrationSectionExt;

public class AdminSection extends AdministrationSectionExt {
	
	private static String requiredPrivileges = "View MiDoctor Settings";
	
	/**
	 * @see Extension@getMediaType()
	 */
	@Override
	public Extension.MEDIA_TYPE getMediaType() {
		return Extension.MEDIA_TYPE.html;
	}
	
	/**
	 * @see AdministrationSectionExt@getTitle()
	 */
	@Override
	public String getTitle() {
		return "midoctorpanel.title";
	}
	
	/**
	 * @see AdministrationSectionExt@getRequiredPrivilege()
	 */
	@Override
	public Map<String, String> getLinks() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("module/midoctorpanel/settings.form", "midoctorpanel.manage.settings");
		return map;
	}
	
	/**
	 * @see AdministrationSectionExt@getRequiredPrivilege()
	 */
	@Override
	public String getRequiredPrivilege() {
		if (requiredPrivileges == null) {
			StringBuilder builder = new StringBuilder();
			requiredPrivileges = builder.toString();
		}
		return requiredPrivileges;
	}
}
