package cl.ehs.openmrs.module.midoctorpanel;

import cl.ehs.openmrs.module.midoctorpanel.task.MidoctorPanelTask;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.api.context.Daemon;
import org.openmrs.module.BaseModuleActivator;
import org.openmrs.module.ModuleActivator;
import org.openmrs.scheduler.*;
import org.openmrs.scheduler.timer.TimerSchedulerTask;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * {@link ModuleActivator} for the midoctorpanel module
 */
public class Activator extends BaseModuleActivator {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	private static final String ENTRY_DATE_TASK_UUID = "fa520661-007e-411c-bbad-85e24ff14a10";
	
	private static final String ENTRY_DATE_TASK_NAME = "Midoctor Panel Entry Date Task";
	
	@Override
	public void started() {
		/*
		log.info("Started the MiDoctorPanel module");
		SchedulerService service = Context.getSchedulerService();
		TaskDefinition task = service.getTaskByName(ENTRY_DATE_TASK_NAME);
		if (task == null) {
			//save task
			task = new TaskDefinition();
			task.setUuid(ENTRY_DATE_TASK_UUID);
			task.setStartOnStartup(false);
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date date = null;
			try {
				date = df.parse("11/20/13 21:00:00");
			}
			catch (ParseException e) {
				log.error(e);
				date = new Date();
			}
			task.setStartTimePattern("MM/dd/yyyy HH:mm:ss");
			task.setStartTime(date);
			//task.setCreatedBy();
			task.setTaskClass(MidoctorPanelTask.class.getName());
			task.setName(ENTRY_DATE_TASK_NAME);
			task.setRepeatInterval(60l);
		}
		
		try {
			if (!task.getStarted())
				service.scheduleTask(task);
		}
		catch (SchedulerException e) {
			e.printStackTrace(); //To change body of catch statement use File | Settings | File Templates.
			log.error(e);
		}
		if (task.getId() <= 0)
			service.saveTaskDefinition(task);
		*/
	}
	
	@Override
	public void stopped() {
		log.info("Stopped the MiDoctorPanel module");
		/*
		SchedulerService service = Context.getSchedulerService();
		TaskDefinition task = service.getTaskByName(ENTRY_DATE_TASK_NAME);
		if (task != null) {
			try {
				service.shutdownTask(task);
				//should we delete task ?
			}
			catch (SchedulerException e) {
				//e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
				log.error(e);
			}
			catch (Exception e) {
				log.error(e);
			}
		}
		*/
	}
}
