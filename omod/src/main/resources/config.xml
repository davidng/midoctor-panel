<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE module PUBLIC "-//OpenMRS//DTD OpenMRS Config 1.0//EN" "http://resources.openmrs.org/doctype/config-1.2.dtd">

<module configVersion="1.2">
    <!-- Base Module Properties -->
    <id>@MODULE_ID@</id>
    <name>@MODULE_NAME@</name>
    <version>@MODULE_VERSION@</version>
    <package>@MODULE_PACKAGE@</package>
    <author>Saptarshi P</author>
    <description>
        Creates an alerts panel in the Find/Create patient page which allows users to more easily see the patients they need to assist
    </description>
    <activator>@MODULE_PACKAGE@.Activator</activator>

    <updateURL>https://modules.openmrs.org/modules/download/midoctorpanel/update.rdf</updateURL>
    <!-- /Base Module Properties -->
	
    <require_version>${openMRSVersion}</require_version>
    <require_modules>
        <require_module>org.openmrs.module.reportingcompatibility</require_module>
        <require_module>org.openmrs.module.reporting</require_module>
    </require_modules>
	
    <!-- Extensions -->
    <extension>
        <point>org.openmrs.admin.list</point>
        <class>cl.ehs.openmrs.module.midoctorpanel.extension.html.AdminSection</class>
    </extension>
    <!--Disabled to remove homepage from showing midoctorHome-->
    <!--extension>
        <point>org.openmrs.navigation.homepage</point>
        <class>cl.ehs.openmrs.module.midoctorpanel.extension.html.HomepageExt</class>
    </extension-->
	
    <!-- AOP -->
	
    <!-- Required Privileges -->
    <privilege>
        <name>View MiDoctor Settings</name>
        <description>Allows user to view MiDoctor Settings.</description>
    </privilege>

    <!-- Required Global Properties -->
    <globalProperty>
        <property>@MODULE_ID@.cohortIdShowList</property>
        <defaultValue></defaultValue>
        <description>A comma separated list of cohortId that should be displayed on Cohort Alert Portlet</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.cohortModuleName</property>
        <defaultValue>reportingcompatibility</defaultValue>
        <description>The global property to set which module is used to get Cohort definitions</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.daysWithinRange</property>
        <defaultValue>0</defaultValue>
        <description>The global property to set no. of days when Cohort is WITHIN_RANGE</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.daysSemiLate</property>
        <defaultValue>5</defaultValue>
        <description>The global property to set no. of days when Cohort is SEMI_LATE</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.daysLate</property>
        <defaultValue>10</defaultValue>
        <description>The global property to set no. of days when Cohort is LATE</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.colorWithinRange</property>
        <defaultValue>00CC33</defaultValue>
        <description>The global property to set color of icon when Cohort is WITHIN_RANGE</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.colorSemiLate</property>
        <defaultValue>FFFF00</defaultValue>
        <description>The global property to set color of icon when Cohort is SEMI_LATE</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.colorLate</property>
        <defaultValue>FF0000</defaultValue>
        <description>The global property to set color of icon when Cohort is LATE</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.cohortAlertPortletBgColor</property>
        <defaultValue>FFFF99</defaultValue>
        <description>The global property to set color of the Cohort Alert Portlet's background</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.patientNameFormat</property>
        <defaultValue>fullName</defaultValue>
        <description>The global property to specify patient name format (fullName, givenName, familyName as comma separated)</description>
    </globalProperty>
    <globalProperty>
        <property>@MODULE_ID@.entryDateQuery</property>
        <defaultValue></defaultValue>
        <description>Matching the cohort query with equivalent report module patient data definition for entry date. Format 1:2;3:4</description>
    </globalProperty>
    <!-- Internationalization -->
    <!-- All message codes should start with moduleId.* -->
    <messages>
        <lang>en</lang>
        <file>messages.properties</file>
    </messages>
    <messages>
	<lang>es</lang>
	<file>messages_es.properties</file>
    </messages>
    <!-- /Internationalization -->

    <mappingFiles>
        ${omodHbmConfig}
        <!--
        <mapping resource="EntryDate.hbm.xml"></mapping>
        -->
    </mappingFiles>
	
	<dwr>
		<allow>
			<create creator="new" javascript="DWRMidoctorService">
				<param name="class" value="cl.ehs.openmrs.module.midoctorpanel.dwr.DWRMidoctorService" />
				<include method="findPatients"/>
				<include method="getPatient"/>
				<include method="getSimilarPatients"/>
				<include method="findDuplicatePatients" />
				<include method="addIdentifier" />
				<include method="exitPatientFromCare" />
				<include method="changeHealthCenter" />
				<include method="createAllergy"/>
				<include method="saveAllergy"/>
				<include method="removeAllergy"/>
				<include method="voidAllergy"/>
				<include method="createProblem"/>
				<include method="saveProblem"/>
				<include method="removeProblem"/>
				<include method="voidProblem"/>
				<include method="findCountAndPatients"/>
				<include method="findBatchOfPatients"/>
			</create>
		</allow>
		
		<signatures>
			<![CDATA[
				import cl.ehs.openmrs.module.midoctorpanel.dwr.DWRMidoctorService;
				DWRMidoctorService.findDuplicatePatients(String[] searchOn);
				DWRMidoctorService.createAllergy(Integer patientId, Integer allergenId, String type, String startDate, String severity, Integer reactionId);
				DWRMidoctorService.saveAllergy(Integer activeListId, Integer allergenId, String type, String startDate, String severity, Integer reactionId);
				DWRMidoctorService.removeAllergy(Integer activeListId, String reason);
				DWRMidoctorService.voidAllergy(Integer activeListId, String reason);
				DWRMidoctorService.createProblem(Integer patientId, Integer problemId, String status, String startDate, String comments);
				DWRMidoctorService.saveProblem(Integer activeListId, Integer problemId, String status, String startDate, String comments);
				DWRMidoctorService.removeProblem(Integer activeListId, String reason, String endDate);
				DWRMidoctorService.voidProblem(Integer activeListId, String reason);
				DWRMidoctorService.findBatchOfPatients(String searchValue, boolean includeVoided, Integer start, Integer length);
				DWRMidoctorService.findPatients(String searchValue, boolean includeVoided);
				DWRMidoctorService.findCountAndPatients(String searchValue, Integer start, Integer length, boolean getMatchCount);
				
			]]>
		</signatures>
	</dwr>
</module>
