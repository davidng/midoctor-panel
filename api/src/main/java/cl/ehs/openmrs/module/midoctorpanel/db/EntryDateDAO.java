package cl.ehs.openmrs.module.midoctorpanel.db;

import java.util.List;

import org.hibernate.SessionFactory;

import cl.ehs.openmrs.module.midoctorpanel.model.EntryDate;

import org.openmrs.Patient;
import org.openmrs.PatientIdentifierType;
import org.openmrs.User;
import org.openmrs.cohort.CohortDefinitionItemHolder;

public interface EntryDateDAO {
	
	public void setSessionFactory(SessionFactory session);
	
	public EntryDate setEntryDate(EntryDate date);
	
	public EntryDate findEntryDate(int patientId, int patientSearchId);
	
	public void deleteEntryDate(EntryDate date);
	
	public void deleteAll();
	
	public List<Patient> getPatients(String name, String identifier, List<PatientIdentifierType> identifierTypes,
	        boolean matchIdentifierExactly, Integer start, Integer length);
	
	public Long getCountOfPatients(String name, String identifier, List<PatientIdentifierType> identifierTypes,
	        boolean matchIdentifierExactly);
	
	public List<Integer> getAssignedHealthCenters(User user);
	
	public List<Integer> getHealthCenters(Patient patient);
}
