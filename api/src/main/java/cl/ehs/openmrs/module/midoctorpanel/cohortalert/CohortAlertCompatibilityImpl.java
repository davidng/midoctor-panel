package cl.ehs.openmrs.module.midoctorpanel.cohortalert;

import cl.ehs.openmrs.module.midoctorpanel.MiDoctorPanelConstants;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Cohort;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Person;
import org.openmrs.Role;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.CohortService;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.LocationService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientIdentifierException;
import org.openmrs.api.context.Context;
import org.openmrs.cohort.CohortDefinitionItemHolder;
import org.openmrs.reporting.PatientSearch;
import org.openmrs.reporting.SearchArgument;
import org.openmrs.util.OpenmrsClassLoader;

/**
 * Class to deal with the implementation of the CohortAlert functionality
 */
public class CohortAlertCompatibilityImpl implements CohortAlertService {
	
	private static Log log = LogFactory.getLog(CohortAlertService.class);
	
	CohortService cohortService;
	
	public CohortAlertCompatibilityImpl() {
		cohortService = Context.getCohortService();
	}
	
	/**
	 * Returns the list of patientSearch cohorts.
	 * 
	 * @returns List<CohortDefinitionItemHolder>
	 */
	@Override
	public List<CohortDefinitionItemHolder> getCohortDefs() {
		List<CohortDefinitionItemHolder> cohortDefs = new LinkedList<CohortDefinitionItemHolder>();
		List<String> cohortIds = new ArrayList<String>();
		/*
		String moduleDependency = Context.getAdministrationService().getGlobalProperty(
		    MiDoctorPanelConstants.COHORT_MODULE_GLOBAL_PROPERTY_NAME);
		
		String cohortIdShowList = Context.getAdministrationService().getGlobalProperty(
		    MiDoctorPanelConstants.COHORT_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME);
		if (cohortIdShowList != null) {
			if (cohortIdShowList.contains(",")) {
				cohortIds = Arrays.asList(cohortIdShowList.replaceAll("\\s", "").split(","));
			} else {
				cohortIds.add(cohortIdShowList.trim());
				*/
		Set<Role> roles = Context.getAuthenticatedUser().getAllRoles();
		Set<String> cohortIdsSet = new TreeSet<String>();
		if (roles != null && !roles.isEmpty()) {
			for (Role role : roles) {
				String corhortIdListString = Context.getAdministrationService().getGlobalProperty(
				    MiDoctorPanelConstants.GLOBAL_PROPERTY_USER_ROLE + role.getRole());
				if (corhortIdListString != null && !corhortIdListString.isEmpty()) {
					if (corhortIdListString.contains(",")) {
						cohortIdsSet.addAll(Arrays.asList(corhortIdListString.replaceAll("\\s", "").split(",")));
					} else {
						cohortIdsSet.add(corhortIdListString.trim());
					}
				}
			}
		}
		if (!cohortIdsSet.isEmpty()) {
			cohortIds.addAll(cohortIdsSet);
		} else {
			String cohortIdShowList = Context.getAdministrationService().getGlobalProperty(
			    MiDoctorPanelConstants.COHORT_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME);
			if (cohortIdShowList != null) {
				if (cohortIdShowList.contains(",")) {
					cohortIds = Arrays.asList(cohortIdShowList.replaceAll("\\s", "").split(","));
				} else {
					cohortIds.add(cohortIdShowList.trim());
				}
				
			}
		}
		
		String moduleDependency = Context.getAdministrationService().getGlobalProperty(
		    MiDoctorPanelConstants.COHORT_MODULE_GLOBAL_PROPERTY_NAME);
		
		if (moduleDependency.equals("reportingcompatibility")) {
			Thread.currentThread().setContextClassLoader(OpenmrsClassLoader.getInstance());
			List<CohortDefinitionItemHolder> allCohortDefs = cohortService.getAllCohortDefinitions();
			for (CohortDefinitionItemHolder c : allCohortDefs) {
				if (c.getKey().contains("org.openmrs.reporting.PatientSearch")) {
					if (cohortIds.contains(c.getKey().split(":")[0])) {
						cohortDefs.add(c);
					}
				}
			}
		}
		return cohortDefs;
	}
	
	/**
	 * Returns list of all the patients inside the given cohort.
	 * 
	 * @param CohortDefinitionItemHolder
	 * @returns List<Patient>
	 */
	@Override
	public List<Patient> getPatientsInCohort(CohortDefinitionItemHolder c) throws PatientIdentifierException {
		List<Patient> patients = new LinkedList<Patient>();
		try {
			Cohort cohort = cohortService.evaluate(c.getCohortDefinition(), null);
			Set<Integer> memberIds = cohort.getMemberIds();
			for (Integer i : memberIds) {
				patients.add(Context.getPatientService().getPatient(i));
			}
		}
		catch (PatientIdentifierException e) {
			log.error(e);
		}
		return patients;
	}
	
	/**
	 * @param String
	 * @returns cohort from given name
	 */
	public Cohort getCohortByName(String name) {
		return cohortService.getCohort(name);
	}
	
	/**
	 * Returns the entry date of the given patient in respective cohort
	 * 
	 * @param CohortDefinitionItemHolder
	 * @param patient
	 * @returns Date
	 */
	@Override
	public Date getPatientEntryDateIntoCohort(CohortDefinitionItemHolder c, Patient patient)
	        throws IllegalArgumentException, ParseException {
		if (c.getKey().contains("org.openmrs.reporting.PatientSearch")) {
			if (getPatientsInCohort(c).contains(patient)) {
				PatientSearch ps = (PatientSearch) c.getCohortDefinition();
				List<SearchArgument> arguments = ps.getArguments();
				if (arguments != null) {
					for (SearchArgument sa : arguments) {
						String searchPropClass = sa.getPropertyClass().getName();
						if (searchPropClass.contains("TimeModifier")) {
							//all obs-based
							ObsService obsService = Context.getObsService();
							ConceptService conceptService = Context.getConceptService();
							Concept concept = null;
							Date withinMonths = null;
							Date withinDays = null;
							Date sinceDate = null;
							Date untilDate = null;
							List<Person> persons = new LinkedList<Person>();
							persons.add(patient);
							for (SearchArgument args : arguments) {
								if (args.getName().equals("value")) {
									if (!args.getValue().equals("")) {
										concept = conceptService.getConcept(Integer.valueOf(args.getValue()));
										continue;
									}
								}
								if (args.getName().equals("withinLastMonths")) {
									Calendar cal = Calendar.getInstance();
									cal.add(Calendar.MONTH, (Integer.valueOf(args.getValue()) * -1));
									withinMonths = cal.getTime();
									continue;
								}
								if (args.getName().equals("withinLastDays")) {
									Calendar cal = Calendar.getInstance();
									cal.add(Calendar.DATE, (Integer.valueOf(args.getValue()) * -1));
									withinDays = cal.getTime();
									continue;
								}
								if (args.getName().equals("sinceDate")) {
									DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
									sinceDate = formatter.parse(args.getValue());
									continue;
								}
								if (args.getName().equals("untilDate")) {
									DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
									untilDate = formatter.parse(args.getValue());
									continue;
								}
								
							}
							if (concept != null) {
								List<Concept> concepts = new LinkedList<Concept>();
								concepts.add(concept);
								List<Obs> obs = new LinkedList<Obs>();
								if (withinMonths == null || withinDays == null) {
									obs = obsService.getObservations(persons, null, null, concepts, null, null, null, null,
									    null, sinceDate, untilDate, false);
								} else if (withinMonths != null) {
									obs = obsService.getObservations(persons, null, null, concepts, null, null, null, null,
									    null, withinMonths, new Date(), false);
								} else if (withinDays != null) {
									obs = obsService.getObservations(persons, null, null, concepts, null, null, null, null,
									    null, withinDays, new Date(), false);
								}
								if (obs.size() > 0) {
									return obs.get(obs.size() - 1).getObsDatetime();
								}
							}
						} else if (searchPropClass.contains("java.util.Date")) {
							//without timemodifierdate
							//Dont know what to do
						} else if (searchPropClass.contains("org.openmrs.EncounterType")) {
							//last encDate at location
							EncounterService es = Context.getEncounterService();
							LocationService ls = Context.getLocationService();
							List<EncounterType> encType = new LinkedList<EncounterType>();
							encType.add(es.getEncounterType(Integer.valueOf(sa.getValue())));
							for (SearchArgument args : arguments) {
								if (args.getPropertyClass().getName().contains("org.openmrs.Location")) {
									List<Encounter> encounters = es.getEncounters(patient,
									    ls.getLocation(Integer.valueOf(args.getValue())), null, null, null, encType, false);
									
									return encounters.get(encounters.size() - 1).getEncounterDatetime();
								}
							}
							List<Encounter> encounters = es.getEncounters(patient, null, null, null, null, encType, false);
							
							return encounters.get(encounters.size() - 1).getEncounterDatetime();
						} else if (searchPropClass.contains("org.openmrs.Program")) {
							PatientProgram patientProgram = Context.getProgramWorkflowService().getPatientProgram(
							    Integer.valueOf(sa.getValue()));
							for (SearchArgument args : arguments) {
								if (args.getName().contains("sinceDate")) {
									if (patientProgram.getCurrentStates().size() > 0) {
										return patientProgram.getCurrentState().getStartDate();
									}
								} else if (args.getName().contains("untilDate")) {
									if (patientProgram.getCurrentStates().size() > 0) {
										return patientProgram.getCurrentState().getEndDate();
									}
								}
							}
							
							return patientProgram.getDateEnrolled();
						} else if (sa.getName().contains("Age") || sa.getName().contains("attribute")
						        || sa.getName().contains("gender") || sa.getName().contains("birthdate")
						        || sa.getName().contains("aliveOnly")) {
							
							return patient.getBirthdate();
						} else if (sa.getName().contains("deadOnly")) {
							
							return patient.getDeathDate();
						} else {
							
							return patient.getDateCreated();
						}
						
					}
				}
				return new Date();
			} else {
				throw new IllegalArgumentException("Patient does not belong in cohort");
			}
		} else {
			throw new IllegalArgumentException("Cohort definition is not valid patient search");
		}
	}
	
	/**
	 * Returns CohortAlertStatus as per the given date.
	 * 
	 * @param Date
	 * @returns CohortAlertStatus enum
	 */
	@Override
	public CohortAlertStatus lateStatusCheck(Date date) {
		AdministrationService as = Context.getAdministrationService();
		Integer daysLate = Integer
		        .valueOf(as.getGlobalProperty(MiDoctorPanelConstants.NO_OF_DAYS_LATE_GLOBAL_PROPERTY_NAME));
		Integer daysSemiLate = Integer.valueOf(as
		        .getGlobalProperty(MiDoctorPanelConstants.NO_OF_DAYS_SEMI_LATE_GLOBAL_PROPERTY_NAME));
		Integer daysWithinRange = Integer.valueOf(as
		        .getGlobalProperty(MiDoctorPanelConstants.NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME));
		
		int dateDiff = DateUtils.getDaysBetweenDates(date, new Date());
		if (dateDiff >= daysLate) {
			return CohortAlertStatus.LATE;
		} else if (dateDiff >= daysSemiLate) {
			return CohortAlertStatus.SEMILATE;
		} else {
			return CohortAlertStatus.WITHINRANGE;
		}
	}
	
	/**
	 * Returns CohortAlertStatus as per the given date.
	 * 
	 * @param dateDiff
	 * @returns CohortAlertStatus enum
	 */
	@Override
	public CohortAlertStatus lateStatusCheck(int dateDiff) {
		AdministrationService as = Context.getAdministrationService();
		Integer daysLate = Integer
		        .valueOf(as.getGlobalProperty(MiDoctorPanelConstants.NO_OF_DAYS_LATE_GLOBAL_PROPERTY_NAME));
		Integer daysSemiLate = Integer.valueOf(as
		        .getGlobalProperty(MiDoctorPanelConstants.NO_OF_DAYS_SEMI_LATE_GLOBAL_PROPERTY_NAME));
		Integer daysWithinRange = Integer.valueOf(as
		        .getGlobalProperty(MiDoctorPanelConstants.NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME));
		
		if (dateDiff >= daysLate) {
			return CohortAlertStatus.LATE;
		} else if (dateDiff >= daysSemiLate) {
			return CohortAlertStatus.SEMILATE;
		} else {
			return CohortAlertStatus.WITHINRANGE;
		}
	}
	
}

class DateUtils {
	
	private static final long MILISECONDS_PER_DAY = 24 * 60 * 60 * 1000;
	
	/**
	 * Returns Number of days between two given dates.
	 * 
	 * @param Date
	 * @param Date
	 * @returns int
	 */
	public static int getDaysBetweenDates(Date startDate, Date endDate) {
		long diff = endDate.getTime() - startDate.getTime();
		int days = (int) Math.floor(diff / MILISECONDS_PER_DAY);
		return Math.abs(days);
	}
}
