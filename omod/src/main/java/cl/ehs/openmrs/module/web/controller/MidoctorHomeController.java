package cl.ehs.openmrs.module.web.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for midoctorpanel's Home portlet which shows patientSearch & cohortAlert.
 */
@Controller("midoctorpanel.MidoctorHomeController")
@RequestMapping("**/findPatient.htm")
public class MidoctorHomeController {
	
	@RequestMapping(method = RequestMethod.GET)
	protected String showView() {
		return "redirect:/module/midoctorpanel/portlets/midoctorHome.htm";
	}
}
