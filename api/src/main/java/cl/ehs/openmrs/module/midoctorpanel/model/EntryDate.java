package cl.ehs.openmrs.module.midoctorpanel.model;

import java.io.Serializable;
import java.util.Date;

import org.openmrs.BaseOpenmrsObject;

public class EntryDate extends BaseOpenmrsObject implements Serializable {
	
	private static final long serialVersionUID = 5109844895265284788L;
	
	private Integer id;
	
	private Integer patientSearchId;
	
	private Long patientId;
	
	private Integer daysSinceEntry = 0;
	
	private Date entryDate;
	
	public Integer getPatientSearchId() {
		return patientSearchId;
	}
	
	public void setPatientSearchId(Integer patientSearchId) {
		this.patientSearchId = patientSearchId;
	}
	
	public Long getPatientId() {
		return patientId;
	}
	
	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}
	
	public Integer getDaysSinceEntry() {
		return daysSinceEntry;
	}
	
	public void setDaysSinceEntry(Integer daysSinceEntry) {
		this.daysSinceEntry = daysSinceEntry;
	}
	
	public Date getEntryDate() {
		return this.entryDate;
	}
	
	public void setEntryDate(Date date) {
		this.entryDate = date;
	}
	
	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
}
