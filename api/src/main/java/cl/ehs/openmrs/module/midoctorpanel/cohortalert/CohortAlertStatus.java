package cl.ehs.openmrs.module.midoctorpanel.cohortalert;

/**
 * Enumeration to define the different status for CohortAlerts
 */
public enum CohortAlertStatus {
	WITHINRANGE, SEMILATE, LATE;
}
