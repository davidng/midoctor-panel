package cl.ehs.openmrs.module.midoctorpanel.db.hibernate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;

import cl.ehs.openmrs.module.midoctorpanel.db.EntryDateDAO;
import cl.ehs.openmrs.module.midoctorpanel.model.EntryDate;

import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Projections;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifierType;
import org.openmrs.Role;
import org.openmrs.User;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.context.Context;
import org.openmrs.api.db.DAOException;
import org.openmrs.util.OpenmrsConstants;

public class HibernateEntryDateDAO implements EntryDateDAO {
	
	private static final int HEALTH_CENTER_TYPE_FIRST_ASSIGNED = 1;
	
	private static final int HEALTH_CENTER_TYPE_LAST_ENCOUNTER = 2;
	
	private static final int HEALTH_CENTER_TYPE_EVER_BELONG = 3;
	
	private AdministrationService adminService;
	
	protected Log log = LogFactory.getLog(getClass());
	
	protected SessionFactory session;
	
	private void getAdminService() {
		if (adminService == null) {
			adminService = Context.getAdministrationService();
		}
	}
	
	@Override
	public void setSessionFactory(SessionFactory session) {
		this.session = session;
	}
	
	@Override
	public EntryDate setEntryDate(EntryDate date) {
		// Coded add to remove duplicate entry
		EntryDate old_entry = findEntryDate((int) (long) (date.getPatientId()), date.getPatientSearchId());
		if (old_entry != null)
			deleteEntryDate(old_entry);
		// End
		session.getCurrentSession().saveOrUpdate(date);
		return date;
	}
	
	@Override
	public void deleteEntryDate(EntryDate date) {
		try {
			session.getCurrentSession().delete(date);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void deleteAll() {
		//To change body of implemented methods use File | Settings | File Templates.
		Transaction trans = session.getCurrentSession().beginTransaction();
		//int result = session.getCurrentSession().createQuery("delete from EntryDate").executeUpdate();
		int result = session.getCurrentSession().createSQLQuery("truncate table midoctor_panel_entry").executeUpdate();
		//log.error("Resutl delete all = " + result);
		trans.commit();
	}
	
	@Override
	public EntryDate findEntryDate(int patientId, int patientSearchId) {
		try {
			return (EntryDate) session
			        .getCurrentSession()
			        .createQuery(
			            "FROM EntryDate AS e WHERE e.patientId = '" + patientId + "' and e.patientSearchId = '"
			                    + patientSearchId + "'").uniqueResult();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Long getCountOfPatients(String name, String identifier, List<PatientIdentifierType> identifierTypes,
	        boolean matchIdentifierExactly) {
		Criteria criteria = session.getCurrentSession().createCriteria(Patient.class);
		criteria = new PatientSearchCriteria(session, criteria).prepareCriteria(name, identifier, identifierTypes,
		    matchIdentifierExactly, false);
		
		// filtered by users
		List<Integer> filteredPatientIds = new ArrayList<Integer>();
		
		getAdminService();
		
		int healthcenterType = 0;
		try {
			healthcenterType = Integer.valueOf(adminService.getGlobalProperty("patientviewrestriction.healthcenter.type"));
		}
		catch (Exception e) {}
		
		User user = Context.getAuthenticatedUser();
		List<Integer> listAssignedHealthCenter = getAssignedHealthCenters(user);
		if (listAssignedHealthCenter.size() > 0 && healthcenterType > 0) {
			StringBuilder builder = new StringBuilder();
			if (healthcenterType == HEALTH_CENTER_TYPE_FIRST_ASSIGNED) {
				builder.append("select e.patient_id from encounter e");
				builder.append(" where (select count(*) from encounter e1 where e.patient_id=e1.patient_id and e1.encounter_datetime<e.encounter_datetime)=0");
				builder.append(" and location_id in (");
				for (int i = 0; i < listAssignedHealthCenter.size(); i++) {
					builder.append(listAssignedHealthCenter.get(i));
					if (i < listAssignedHealthCenter.size() - 1)
						builder.append(",");
				}
				builder.append(")");
			} else if (healthcenterType == HEALTH_CENTER_TYPE_LAST_ENCOUNTER) {
				builder.append("select e.patient_id from encounter e");
				builder.append(" where (select count(*) from encounter e1 where e.patient_id=e1.patient_id and e1.encounter_datetime>e.encounter_datetime)=0");
				builder.append(" and location_id in (");
				for (int i = 0; i < listAssignedHealthCenter.size(); i++) {
					builder.append(listAssignedHealthCenter.get(i));
					if (i < listAssignedHealthCenter.size() - 1)
						builder.append(",");
				}
				builder.append(")");
			} else if (healthcenterType == HEALTH_CENTER_TYPE_EVER_BELONG) {
				builder.append("select e.patient_id from encounter e");
				builder.append(" where location_id in (");
				for (int i = 0; i < listAssignedHealthCenter.size(); i++) {
					builder.append(listAssignedHealthCenter.get(i));
					if (i < listAssignedHealthCenter.size() - 1)
						builder.append(",");
				}
				builder.append(")");
			}
			
			String sql = builder.toString();
			if (!sql.isEmpty()) {
				SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List records = query.list();
				for (Object obj : records) {
					Map map = (Map) obj;
					Integer patientId = (Integer) map.get("patient_id");
					if (!filteredPatientIds.contains(patientId))
						filteredPatientIds.add(patientId);
				}
			}
		}
		
		if (filteredPatientIds.size() == 0)
			return 0l;
		
		criteria.add(Expression.in("patientId", filteredPatientIds));
		// end filter
		
		criteria.setProjection(Projections.countDistinct("patientId"));
		return (Long) criteria.uniqueResult();
	}
	
	protected Integer getMaximumSearchResults() {
		try {
			return Integer.valueOf(Context.getAdministrationService().getGlobalProperty(
			    OpenmrsConstants.GLOBAL_PROPERTY_PERSON_SEARCH_MAX_RESULTS,
			    String.valueOf(OpenmrsConstants.GLOBAL_PROPERTY_PERSON_SEARCH_MAX_RESULTS_DEFAULT_VALUE)));
		}
		catch (Exception e) {
			log.warn("Unable to convert the global property " + OpenmrsConstants.GLOBAL_PROPERTY_PERSON_SEARCH_MAX_RESULTS
			        + "to a valid integer. Returning the default "
			        + OpenmrsConstants.GLOBAL_PROPERTY_PERSON_SEARCH_MAX_RESULTS_DEFAULT_VALUE);
		}
		
		return OpenmrsConstants.GLOBAL_PROPERTY_PERSON_SEARCH_MAX_RESULTS_DEFAULT_VALUE;
	}
	
	public List<Patient> getPatients(String name, String identifier, List<PatientIdentifierType> identifierTypes,
	        boolean matchIdentifierExactly, Integer start, Integer length) throws DAOException {
		if (StringUtils.isBlank(name) && StringUtils.isBlank(identifier)
		        && (identifierTypes == null || identifierTypes.isEmpty())) {
			return Collections.emptyList();
		}
		
		Criteria criteria = session.getCurrentSession().createCriteria(Patient.class);
		criteria = new PatientSearchCriteria(session, criteria).prepareCriteria(name, identifier, identifierTypes,
		    matchIdentifierExactly, true);
		
		// filtered by users
		List<Integer> filteredPatientIds = new ArrayList<Integer>();
		
		getAdminService();
		
		int healthcenterType = 1;
		try {
			healthcenterType = Integer.valueOf(adminService.getGlobalProperty("patientviewrestriction.healthcenter.type"));
		}
		catch (Exception e) {}
		
		User user = Context.getAuthenticatedUser();
		List<Integer> listAssignedHealthCenter = getAssignedHealthCenters(user);
		if (listAssignedHealthCenter.size() > 0 && healthcenterType > 0) {
			StringBuilder builder = new StringBuilder();
			if (healthcenterType == HEALTH_CENTER_TYPE_FIRST_ASSIGNED) {
				builder.append("select e.patient_id from encounter e");
				builder.append(" where (select count(*) from encounter e1 where e.patient_id=e1.patient_id and e1.encounter_datetime<e.encounter_datetime)=0");
				builder.append(" and location_id in (");
				for (int i = 0; i < listAssignedHealthCenter.size(); i++) {
					builder.append(listAssignedHealthCenter.get(i));
					if (i < listAssignedHealthCenter.size() - 1)
						builder.append(",");
				}
				builder.append(")");
			} else if (healthcenterType == HEALTH_CENTER_TYPE_LAST_ENCOUNTER) {
				builder.append("select e.patient_id from encounter e");
				builder.append(" where (select count(*) from encounter e1 where e.patient_id=e1.patient_id and e1.encounter_datetime>e.encounter_datetime)=0");
				builder.append(" and location_id in (");
				for (int i = 0; i < listAssignedHealthCenter.size(); i++) {
					builder.append(listAssignedHealthCenter.get(i));
					if (i < listAssignedHealthCenter.size() - 1)
						builder.append(",");
				}
				builder.append(")");
			} else if (healthcenterType == HEALTH_CENTER_TYPE_EVER_BELONG) {
				builder.append("select e.patient_id from encounter e");
				builder.append(" where location_id in (");
				for (int i = 0; i < listAssignedHealthCenter.size(); i++) {
					builder.append(listAssignedHealthCenter.get(i));
					if (i < listAssignedHealthCenter.size() - 1)
						builder.append(",");
				}
				builder.append(")");
			}
			
			String sql = builder.toString();
			if (!sql.isEmpty()) {
				SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List records = query.list();
				for (Object obj : records) {
					Map map = (Map) obj;
					Integer patientId = (Integer) map.get("patient_id");
					if (!filteredPatientIds.contains(patientId))
						filteredPatientIds.add(patientId);
				}
			}
		}
		
		if (filteredPatientIds.size() == 0)
			return new ArrayList<Patient>();
		
		criteria.add(Expression.in("patientId", filteredPatientIds));
		
		// end filter
		
		// restricting the search to the max search results value
		if (start != null)
			criteria.setFirstResult(start);
		int limit = getMaximumSearchResults();
		if (length == null || length > limit) {
			if (log.isDebugEnabled())
				log.debug("Limitng the size of the number of matching patients to " + limit);
			length = limit;
		}
		if (length != null)
			criteria.setMaxResults(length);
		
		return criteria.list();
	}
	
	@Override
	public List<Integer> getAssignedHealthCenters(User user) {
		List<Integer> result = new ArrayList<Integer>();
		List<String> listRole = new ArrayList<String>();
		for (Role role : user.getAllRoles()) {
			listRole.add(role.getRole());
		}
		
		String sql = "select * from healthcenters";
		SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		List records = query.list();
		for (Object obj : records) {
			Map map = (Map) obj;
			Integer locationId = (Integer) map.get("location_id");
			String userIds = (String) map.get("user_ids");
			String roleIds = (String) map.get("role_ids");
			String[] userArr = userIds.split(",");
			String[] roleArr = roleIds.split(",");
			if (locationId == null)
				continue;
			
			boolean assigned = false;
			for (String st : userArr) {
				if (user.getUserId().toString().equals(st)) {
					assigned = true;
					break;
				}
			}
			
			if (!assigned) {
				for (String st : roleArr) {
					if (st.isEmpty())
						continue;
					if (listRole.contains(st)) {
						assigned = true;
						break;
					}
				}
			}
			
			if (assigned) {
				if (!result.contains(locationId))
					result.add(locationId);
			}
		}
		return result;
	}
	
	@Override
	public List<Integer> getHealthCenters(Patient patient) {
		List<Integer> result = new ArrayList<Integer>();
		
		getAdminService();
		
		int healthcenterType = 1;
		try {
			healthcenterType = Integer.valueOf(adminService.getGlobalProperty("patientviewrestriction.healthcenter.type"));
		}
		catch (Exception e) {}
		
		if (healthcenterType <= 0)
			return result;
		
		if (healthcenterType == HEALTH_CENTER_TYPE_FIRST_ASSIGNED) {
			String sql = "select location_id from encounter where patient_id = " + patient.getPatientId()
			        + " order by encounter_datetime asc";
			SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List records = query.list();
			if (records.size() > 0) {
				Map map = (Map) (records.get(0));
				result.add((Integer) map.get("location_id"));
			}
		} else if (healthcenterType == HEALTH_CENTER_TYPE_LAST_ENCOUNTER) {
			String sql = "select location_id from encounter where patient_id = " + patient.getPatientId()
			        + " order by encounter_datetime desc";
			SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List records = query.list();
			if (records.size() > 0) {
				Map map = (Map) (records.get(0));
				result.add((Integer) map.get("location_id"));
			}
		} else if (healthcenterType == HEALTH_CENTER_TYPE_EVER_BELONG) {
			String sql = "select location_id from encounter where patient_id = " + patient.getPatientId()
			        + " order by encounter_datetime desc";
			SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List records = query.list();
			for (Object obj : records) {
				Map map = (Map) obj;
				Integer locationId = (Integer) map.get("location_id");
				if (!result.contains(locationId))
					result.add(locationId);
			}
		}
		
		return result;
	}
}
