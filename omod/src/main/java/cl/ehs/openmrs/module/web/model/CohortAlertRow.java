package cl.ehs.openmrs.module.web.model;

import cl.ehs.openmrs.module.midoctorpanel.cohortalert.CohortAlertStatus;
import java.util.Date;
import org.openmrs.Patient;

public class CohortAlertRow {
	
	private Date cohortEntryDate;
	
	private Patient patient;
	
	private CohortAlertStatus status;
	
	public CohortAlertRow() {
		
	}
	
	public Date getCohortEntryDate() {
		return cohortEntryDate;
	}
	
	public void setCohortEntryDate(Date cohortEntryDate) {
		this.cohortEntryDate = cohortEntryDate;
	}
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	public CohortAlertStatus getStatus() {
		return status;
	}
	
	public void setStatus(CohortAlertStatus status) {
		this.status = status;
	}
}
