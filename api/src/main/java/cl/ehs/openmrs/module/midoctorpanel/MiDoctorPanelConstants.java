package cl.ehs.openmrs.module.midoctorpanel;

/**
 * Constants used by the MiDoctorPanel module
 */
public class MiDoctorPanelConstants {
	
	//module id or name
	public static final String MODULE_ID = "midoctorpanel";
	
	// <editor-fold defaultstate="collapsed" desc="Global Properties Default Values">
	/**
	 * The moduleid to use for getting cohort definitions
	 * 
	 * @see #COHORT_MODULE_GLOBAL_PROPERTY_NAME
	 */
	public static String COHORT_MODULE = "reportingcompatibility";
	
	/**
	 * The number of days when cohorts are classified within range
	 * 
	 * @see #NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME
	 */
	public static Integer NO_OF_DAYS_WITHIN_RANGE = 0;
	
	/**
	 * The number of days when cohorts are classified semi_late
	 * 
	 * @see #NO_OF_DAYS_SEMI_LATE_GLOBAL_PROPERTY_NAME
	 */
	public static Integer NO_OF_DAYS_SEMI_LATE = 5;
	
	/**
	 * The number of days when cohorts are classified as late
	 * 
	 * @see #NO_OF_DAYS_LATE_GLOBAL_PROPERTY_NAME
	 */
	public static Integer NO_OF_DAYS_LATE = 10;
	
	/**
	 * The HTML color code to display when cohorts WITHIN_RANGE
	 * 
	 * @see #WITHIN_RANGE_COLOR_GLOBAL_PROPERTY_NAME
	 */
	public static String WITHIN_RANGE_COLOR = "00CC33";
	
	/**
	 * The HTML color code to display when cohorts SEMI_LATE
	 * 
	 * @see #SEMI_LATE_COLOR_GLOBAL_PROPERTY_NAME
	 */
	public static String SEMI_LATE_COLOR = "FFFF00";
	
	/**
	 * The HTML color code to display when cohorts LATE
	 * 
	 * @see #LATE_COLOR_GLOBAL_PROPERTY_NAME
	 */
	public static String LATE_COLOR = "FF0000";
	
	/**
	 * The HTML color code to display for the Cohort Alert Portlet's background
	 * 
	 * @see #COHORT_ALERT_PORTLET_BGCOLOR_GLOBAL_PROPERTY_NAME
	 */
	public static String COHORT_ALERT_PORTLET_BGCOLOR = "FFFF99";
	
	/**
	 * The format in which patient name should be displayed on the cohort alert portlet
	 * 
	 * @see #PATIENT_NAME_FORMAT_GLOBAL_PROPERTY_NAME
	 */
	public static String PATIENT_NAME_FORMAT = "fullName";
	
	//</editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Global Properties key names">
	/**
	 * The key for the global property to set list of cohortIds that should be visible
	 * 
	 * @see #COHORT_MODULE
	 */
	public static String COHORT_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME = MODULE_ID + ".cohortIdShowList";
	
	/**
	 * The key for the global property to set which role of the user is used to get Cohort
	 * definitions
	 */
	public static String GLOBAL_PROPERTY_USER_ROLE = MODULE_ID + ".role.";
	
	/**
	 * The key for the global property to set which module is used to get Cohort definitions
	 * 
	 * @see #COHORT_MODULE
	 */
	public static String COHORT_MODULE_GLOBAL_PROPERTY_NAME = MODULE_ID + ".cohortModuleName";
	
	/**
	 * The key for the global property to set no. of days when Cohort is WITHIN_RANGE
	 * 
	 * @see #NO_OF_DAYS_WITHIN_RANGE
	 */
	public static String NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME = MODULE_ID + ".daysWithinRange";
	
	/**
	 * The key for the global property to set no. of days when Cohort is SEMI_LATE
	 * 
	 * @see #NO_OF_DAYS_SEMI_LATE
	 */
	public static String NO_OF_DAYS_SEMI_LATE_GLOBAL_PROPERTY_NAME = MODULE_ID + ".daysSemiLate";
	
	/**
	 * The key for the global property to set no. of days when Cohort is LATE
	 * 
	 * @see #NO_OF_DAYS_LATE
	 */
	public static String NO_OF_DAYS_LATE_GLOBAL_PROPERTY_NAME = MODULE_ID + ".daysLate";
	
	/**
	 * The key for the global property to set color of icon when Cohort is WITHIN_RANGE
	 * 
	 * @see #WITHIN_RANGE_COLOR
	 */
	public static String WITHIN_RANGE_COLOR_GLOBAL_PROPERTY_NAME = MODULE_ID + ".colorWithinRange";
	
	/**
	 * The key for the global property to set color of icon when Cohort is SEMI_LATE
	 * 
	 * @see #SEMI_LATE_COLOR
	 */
	public static String SEMI_LATE_COLOR_GLOBAL_PROPERTY_NAME = MODULE_ID + ".colorSemiLate";
	
	/**
	 * The key for the global property to set color of icon when Cohort is LATE
	 * 
	 * @see #LATE_COLOR
	 */
	public static String LATE_COLOR_GLOBAL_PROPERTY_NAME = MODULE_ID + ".colorLate";
	
	/**
	 * The key for the global property to set color of the Cohort Alert Portlet's background
	 * 
	 * @see #COHORT_ALERT_PORTLET_BGCOLOR
	 */
	public static String COHORT_ALERT_PORTLET_BGCOLOR_GLOBAL_PROPERTY_NAME = MODULE_ID + ".cohortAlertPortletBgColor";
	
	/**
	 * The key for the global property to set patient name format
	 * 
	 * @see #PATIENT_NAME_FORMAT
	 */
	public static String PATIENT_NAME_FORMAT_GLOBAL_PROPERTY_NAME = MODULE_ID + ".patientNameFormat";
	
	/**
	 * Matching the cohort query with equivalent report module patient data definition for entry
	 * date. Example format 1:2;3:4
	 */
	public static String COHORT_ENTRY_DATE_REPORT_QUERY_GLOBAL_PROPERTY_NAME = MODULE_ID + ".entryDateQuery";
	// </editor-fold> 
}
