<ul id="menu">
    <li class="first">
        <a href="${pageContext.request.contextPath}/admin"><spring:message code="admin.title.short"/></a>
    </li>

    <openmrs:hasPrivilege privilege="Manage Global Properties">
        <li <c:if test='<%= request.getRequestURI().contains("midoctorpanel/settings") %>'>class="active"</c:if>>
                <a href="settings.form">
                <spring:message code="midoctorpanel.manage.settings"/>
            </a>
        </li>
    </openmrs:hasPrivilege>

    <openmrs:extensionPoint pointId="org.openmrs.module.midoctorpanel.admin.localHeader" type="html">
        <c:forEach items="${extension.links}" var="link">
            <li <c:if test="${fn:endsWith(pageContext.request.requestURI, link.key)}">class="active"</c:if> >
                <a href="${pageContext.request.contextPath}/${link.key}"><spring:message code="${link.value}"/></a>
            </li>
        </c:forEach>
    </openmrs:extensionPoint>
</ul>