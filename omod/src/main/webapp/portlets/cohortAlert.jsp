<%@ include file="/WEB-INF/template/include.jsp" %>
<c:set var="DO_NOT_INCLUDE_JQUERY" value="true"/>
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/jquery-1.8.3.min.js" />
<script>
    $j = jQuery.noConflict();
</script>
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/jquery-ui-1.9.2.custom.min.js" />
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/styles/jquery-ui-1.9.2.custom.min.css" />
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/styles/midoctorHome.css" />
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/themes/default/style.css" />
<openmrs:htmlInclude file="/moduleResources/midoctorpanel/jquery.jstree.js" />

<script>
    $j(document).ready(function() {

        //These are used to put values on the buttons
        var withinRangeCount = $j('.WITHINRANGE').length;
        var lateCount = $j('.LATE').length;
        var semiLateCount = $j('.SEMILATE').length;
        var totalCount = withinRangeCount + lateCount + semiLateCount;

        //These are used to calculate the percentages of the status for coloring
        var latePercent = (lateCount / totalCount) * 100;
        var semiLatePercent = (semiLateCount / totalCount) * 100;
        var withinRangePercent = (withinRangeCount / totalCount) * 100;

        //Using CSS3 Gradient to get the different colors on button background
        /*var graphStyle = 'margin-left: 25px; width: 60px; ' +
                'background: -moz-linear-gradient(left, ' +
                '#<openmrs:globalProperty key="midoctorpanel.colorLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + semiLatePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorWithinRange"/> ' + withinRangePercent + '%);' + /* FF3.6+ */
                /*'background: -webkit-linear-gradient(left, ' +
                '#<openmrs:globalProperty key="midoctorpanel.colorLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + semiLatePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorWithinRange"/> ' + withinRangePercent + '%);' + /* Chrome10+,Safari5.1+ */
                /*'background: linear-gradient(to right, ' +
                '#<openmrs:globalProperty key="midoctorpanel.colorLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + semiLatePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorWithinRange"/> ' + withinRangePercent + '%);' + /* W3C */
                /*'background: -o-linear-gradient(left, ' +
                '#<openmrs:globalProperty key="midoctorpanel.colorLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + semiLatePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorWithinRange"/> ' + withinRangePercent + '%);' + /* Opera 11.10+ */
                /*'background: -ms-linear-gradient(left, ' +
                '#<openmrs:globalProperty key="midoctorpanel.colorLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + latePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/> ' + semiLatePercent + '%,' +
                '#<openmrs:globalProperty key="midoctorpanel.colorWithinRange"/> ' + withinRangePercent + '%);'; /* IE10+ */

        /*Applies the gradient style to the totalButton*/
        /*$j("#totalButton").attr('style', graphStyle);*/

        //Applies the calculated numbers on the buttons

        $j('#withinRangeButton').attr('value', withinRangeCount);
        $j('#lateButton').attr('value', lateCount);
        $j('#semiLateButton').attr('value', semiLateCount);
        $j('#totalButton').attr('value', totalCount);

        //Puts the create patient button on the patient search portlet
        if ($j("#createPatientButton").length == 0) {
            $j("#inputNode").after('<input type="button" id="createPatientButton" value="<spring:message code="Person.create"/>" onclick="location.href=\'' + openmrsContextPath + '/admin/patients/shortPatientForm.form\'" />');
        }

        $j("#treeDiv").jstree({
            "plugins": ["themes", "html_data"],
            "themes": {"theme": "default", "dots": true, "icons": false}
        }).bind("loaded.jstree", function(event, data) {
            // you get two params - event & data - check the core docs for a detailed description
            $j(this).jstree("open_all");
        });

        $j('#lateButton').click(function() {
            $j('.WITHINRANGE').show();
            $j('.LATE').show();
            $j('.SEMILATE').show();

            $j('.WITHINRANGE').hide();
            $j('.SEMILATE').hide();
        });

        $j('#withinRangeButton').click(function() {
            $j('.WITHINRANGE').show();
            $j('.LATE').show();
            $j('.SEMILATE').show();

            $j('.LATE').hide();
            $j('.SEMILATE').hide();
        });

        $j('#semiLateButton').click(function() {
            $j('.WITHINRANGE').show();
            $j('.LATE').show();
            $j('.SEMILATE').show();

            $j('.WITHINRANGE').hide();
            $j('.LATE').hide();
        });

        $j('#totalButton').click(function() {
            $j('.WITHINRANGE').show();
            $j('.LATE').show();
            $j('.SEMILATE').show();
        });

        // Expand All
        $j('#collapseExpandAll').click(function() {
            if ($j(this).attr('class') == 'expandAll')
            {
                $j(this).removeClass("expandAll").addClass("collapseAll");
                $j('.WITHINRANGE').show();
                $j('.LATE').show();
                $j('.SEMILATE').show();
                $j('#treeDiv').jstree('open_all');
            }
            else
            {
                $j(this).removeClass("collapseAll").addClass("expandAll");
                $j('.WITHINRANGE').show();
                $j('.LATE').show();
                $j('.SEMILATE').show();
                $j('#treeDiv').jstree('close_all');
            }
        });
    });
</script>
<div id="cohortAlert">
    <b class="boxHeader" style="padding-bottom: 10px">
        <input type="button" id='collapseExpandAll' class="collapseAll" />
        <spring:message code="midoctorpanel.portalHeader"/>
        <div id="buttonsDiv" style="float: right">
            <input type="button" value="0" id='totalButton' />
            <input style='margin-left: 25px; background-color:#<openmrs:globalProperty key="midoctorpanel.colorLate"/>' type="button" value="0" id='lateButton' />
            <input style='margin-left: 25px; background-color:#<openmrs:globalProperty key="midoctorpanel.colorSemiLate"/>' type="button" value="0" id='semiLateButton' />
            <input style='margin-left: 25px; background-color:#<openmrs:globalProperty key="midoctorpanel.colorWithinRange"/>' type="button" value="0" id='withinRangeButton' />

         </div>
    </b>
    <div class="box">
        <form>
            <div id='treeDiv' style="background-color:#<openmrs:globalProperty key="midoctorpanel.cohortAlertPortletBgColor"/>">
                <ul>
                    <c:forEach var="units" items="${cohortAlertModel}" varStatus="rowCounter">
                        <c:if test="${!empty units.rows}"> 
                            <li item-expanded='true'><a href="${pageContext.request.contextPath}/cohortBuilder.form?method=addFilter&search_id=${units.cohortId}">${units.cohortName}</a>
                                <ul>
                                    <c:forEach var="row" items="${units.rows}" varStatus="rowCounter">
                                        <li class='${row.status}'>
                                            <a href="${pageContext.request.contextPath}/patientDashboard.form?patientId=${row.patient.id}">
                                                <div style="background-color:#<openmrs:globalProperty key="midoctorpanel.color${row.status}"/>;height: 16px;float: left; width: 16px; margin-right:10px; border-radius: 16px;">
                                                   <span style="margin-left:20px">
                                                        <c:if test="${row.cohortEntryDate!= null}">
                                                            <openmrs:formatDate date="${row.cohortEntryDate}"/>,
                                                        </c:if>
                                                        <c:set var="patientNameFormat"><openmrs:globalProperty key='midoctorpanel.patientNameFormat'/></c:set>
                                                    </span>
                                                    <c:choose>
                                                        <c:when test="${patientNameFormat == 'fullName' || empty patientNameFormat}">
                                                            ${row.patient.personName}
                                                        </c:when>
                                                        <c:when test="${fn:contains(patientNameFormat, ',')}">
                                                            <c:set var="nameParts" value="${fn:split(patientNameFormat, ',')}" />
                                                            <c:forEach var="part" items="${nameParts}">
                                                                <c:choose>
                                                                    <c:when test="${fn:trim(part) == 'givenName'}">
                                                                        ${row.patient.personName.givenName}
                                                                    </c:when>
                                                                    <c:when test="${fn:trim(part) == 'middleName'}">
                                                                        ${row.patient.personName.middleName}
                                                                    </c:when>
                                                                    <c:when test="${fn:trim(part) == 'familyName'}">
                                                                        ${row.patient.personName.familyName}
                                                                    </c:when>
                                                                    <c:when test="${fn:trim(part) == 'familyName2'}">
                                                                        ${row.patient.personName.familyName2}
                                                                    </c:when>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:choose>
                                                                <c:when test="${patientNameFormat == 'givenName'}">
                                                                    ${row.patient.personName.givenName}
                                                                </c:when>
                                                                <c:when test="${patientNameFormat == 'middleName'}">
                                                                    ${row.patient.personName.middleName}
                                                                </c:when>
                                                                <c:when test="${patientNameFormat == 'familyName'}">
                                                                    ${row.patient.personName.familyName}
                                                                </c:when>
                                                                <c:when test="${patientNameFormat == 'familyName2'}">
                                                                    ${row.patient.personName.familyName2}
                                                                </c:when>
                                                            </c:choose>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                            </a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                        </c:if>
                    </c:forEach>
                </ul>
            </div>
        </form>
    </div>
</div>