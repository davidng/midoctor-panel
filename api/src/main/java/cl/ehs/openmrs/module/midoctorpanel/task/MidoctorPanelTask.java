package cl.ehs.openmrs.module.midoctorpanel.task;

import cl.ehs.openmrs.module.midoctorpanel.MiDoctorPanelConstants;
import cl.ehs.openmrs.module.midoctorpanel.api.EntryDateService;
import cl.ehs.openmrs.module.midoctorpanel.cohortalert.CohortAlertCompatibilityImpl;
import cl.ehs.openmrs.module.midoctorpanel.cohortalert.CohortAlertService;
import cl.ehs.openmrs.module.midoctorpanel.model.EntryDate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Cohort;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.cohort.CohortDefinitionItemHolder;
import org.openmrs.module.reporting.data.patient.EvaluatedPatientData;
import org.openmrs.module.reporting.data.patient.definition.PatientDataDefinition;
import org.openmrs.module.reporting.data.patient.service.PatientDataService;
import org.openmrs.module.reporting.dataset.DataSetRow;
import org.openmrs.module.reporting.dataset.SimpleDataSet;
import org.openmrs.module.reporting.dataset.definition.SqlDataSetDefinition;
import org.openmrs.module.reporting.dataset.definition.service.DataSetDefinitionService;
import org.openmrs.module.reporting.definition.DefinitionContext;
import org.openmrs.module.reporting.evaluation.EvaluationContext;
import org.openmrs.module.reporting.evaluation.EvaluationException;
import org.openmrs.scheduler.tasks.AbstractTask;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class MidoctorPanelTask extends AbstractTask {
	
	private Log log = LogFactory.getLog(MidoctorPanelTask.class);
	
	private EntryDateService service;
	
	private static Boolean isRunning = false;
	
	@Override
	public void execute() {
		
		synchronized (isRunning) {
			if (isRunning) {
				log.warn("Midoctor panel task aborting (another processor already running)");
				return;
			}
			isRunning = true;
		}
		
		log.info("Starting Midoctor panel task...");
		Context.openSession();
		service = Context.getService(EntryDateService.class);
		
		service.clearEntryDateTable();
		
		CohortAlertService cas = new CohortAlertCompatibilityImpl();//Context.getService(CohortAlertService.class);
		String entryDateQuery = Context.getAdministrationService().getGlobalProperty(
		    MiDoctorPanelConstants.COHORT_ENTRY_DATE_REPORT_QUERY_GLOBAL_PROPERTY_NAME);
		
		//log.error("entry date query = " + entryDateQuery);
		if (entryDateQuery == null) {
			isRunning = false;
			return;
		}
		
		Map<Integer, Integer> defMap = new Hashtable<Integer, Integer>();
		for (String token : entryDateQuery.split(";")) {
			String[] parts = token.split(":");
			//log.error("token 0: " + Integer.parseInt(parts[0]));
			defMap.put(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
		}
		
		for (CohortDefinitionItemHolder item : cas.getCohortDefs()) {
			int cohortId = Integer.parseInt(item.getKey().split(":")[0].trim());
			Integer dataDef = defMap.get(cohortId);
			//log.error("dataDef: " + dataDef.toString());
			if (dataDef != null) {
				//get all patients in that cohort
				List<Patient> patients = cas.getPatientsInCohort(item);
				
				EvaluationContext context = new EvaluationContext();
				/*
				PatientDataDefinition def = DefinitionContext.getDefinitionService(PatientDataDefinition.class)
				        .getDefinition(PatientDataDefinition.class, dataDef);
				
				StringBuilder b = new StringBuilder();
				for (int i = 0; i < patients.size(); i++) {
					if (i > 0)
						b.append(",");
					b.append(patients.get(i).getId());
				}
				context.setBaseCohort(new Cohort(b.toString()));
				try {
					EvaluatedPatientData pd = Context.getService(PatientDataService.class).evaluate(def, context);
					for (Patient p : patients) {
						Date entryDate = (Date) pd.getData().get(p.getId());
						EntryDate ed = new EntryDate();
						long days = (new Date().getTime() - entryDate.getTime()) / (1000 * 60 * 60 * 24);
						ed.setDaysSinceEntry((int) days);
						ed.setPatientId(p.getId());
						ed.setPatientSearchId(cohortId);
						service.saveEntryDate(ed);
					}
				}
				catch (EvaluationException e) {
					log.error(e);
				}
				*/
				try {
					SqlDataSetDefinition def = DefinitionContext.getDefinitionService(SqlDataSetDefinition.class)
					        .getDefinition(SqlDataSetDefinition.class, dataDef);
					//log.error("def: " + def.toString());
					if (def != null) {
						try {
							SimpleDataSet result = (SimpleDataSet) Context.getService(DataSetDefinitionService.class)
							        .evaluate(def, context);
							for (int r = 0; r < result.getRows().size(); r++) {
								//log.error("r: " + r);
								DataSetRow row = result.getRows().get(r);
								EntryDate entryDate = new EntryDate();
								//log.error("entry-date-1");
								Object val = row.getColumnValue("patient_id");
								if (val instanceof Integer) {
									//log.error("entry-date-2");
									entryDate.setPatientId(((Integer) val).longValue());
								} else {
									//log.error("entry-date-3");
									entryDate.setPatientId((Long) val);
								}
								//do not continue if patient id is not set
								if (entryDate.getPatientId() == null) {
									log.error("Patient Id is empty: " + row.getColumnValue("patient_id"));
									continue;
								}
								//log.error("entry-date-4");
								entryDate.setPatientSearchId(cohortId);
								val = row.getColumnValue("days_since_entry");
								if (val instanceof Integer) {
									//log.error("entry-date-5");
									entryDate.setDaysSinceEntry((Integer) val);
								} else {
									//log.error("entry-date-6");
									entryDate.setDaysSinceEntry(((Long) val).intValue());
								}
								//log.error("entry-date-7");
								
								entryDate.setEntryDate((Date) row.getColumnValue("entry_date"));
								//log.error("entry-date-8");
								service.saveEntryDate(entryDate);
								//log.error("entry-date-9");
							}
							
							//testing
							/*
							EntryDate entryDate = new EntryDate();
							entryDate.setPatientId(3);
							entryDate.setPatientSearchId(1);
							entryDate.setDaysSinceEntry(1000);
							entryDate.setEntryDate(new Date());
							service.saveEntryDate(entryDate);
							*/
						}
						catch (EvaluationException e) {
							log.error(e);
						}
					}
				}
				catch (Exception e) {
					log.error(e);
					
				}
			}
			
		}
		isRunning = false;
	}
	
}
