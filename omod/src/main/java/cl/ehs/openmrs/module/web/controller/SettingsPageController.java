package cl.ehs.openmrs.module.web.controller;

import cl.ehs.openmrs.module.midoctorpanel.MiDoctorPanelConstants;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.openmrs.GlobalProperty;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.context.Context;
import org.openmrs.web.WebConstants;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

/**
 * Controller for midoctorpanel's "settings.jsp" page.
 */
@Controller("midoctorpanel.SettingsPageController")
@RequestMapping("/module/midoctorpanel/settings.form")
public class SettingsPageController {
	
	@RequestMapping(method = RequestMethod.GET)
	public void showForm() {
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String handleSubmission(@ModelAttribute("globalPropertiesModel") GlobalPropertiesModel globalPropertiesModel,
	        Errors errors, WebRequest request) {
		globalPropertiesModel.validate(globalPropertiesModel, errors);
		if (errors.hasErrors()) {
			return null; // show the form again
		}
		AdministrationService administrationService = Context.getAdministrationService();
		for (GlobalProperty p : globalPropertiesModel.getProperties()) {
			administrationService.saveGlobalProperty(p);
		}
		
		request.setAttribute(WebConstants.OPENMRS_MSG_ATTR, Context.getMessageSourceService().getMessage("general.saved"),
		    WebRequest.SCOPE_SESSION);
		return "redirect:settings.form";
	}
	
	/**
	 * Returns GlobalPropertiesModel with all the editable global properties
	 * 
	 * @return GlobalPropertiesModel
	 */
	@ModelAttribute("globalPropertiesModel")
	public GlobalPropertiesModel getModel() {
		List<GlobalProperty> editableProps = new ArrayList<GlobalProperty>();
		
		Set<String> props = new LinkedHashSet<String>();
		props.add(MiDoctorPanelConstants.COHORT_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.COHORT_MODULE_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.NO_OF_DAYS_SEMI_LATE_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.NO_OF_DAYS_LATE_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.WITHIN_RANGE_COLOR_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.SEMI_LATE_COLOR_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.LATE_COLOR_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.COHORT_ALERT_PORTLET_BGCOLOR_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.PATIENT_NAME_FORMAT_GLOBAL_PROPERTY_NAME);
		props.add(MiDoctorPanelConstants.COHORT_ENTRY_DATE_REPORT_QUERY_GLOBAL_PROPERTY_NAME);
		
		//remove the properties we dont want to edit
		for (GlobalProperty gp : Context.getAdministrationService().getGlobalPropertiesByPrefix(
		    MiDoctorPanelConstants.MODULE_ID)) {
			if (props.contains(gp.getProperty())) {
				editableProps.add(gp);
			}
		}
		return new GlobalPropertiesModel(editableProps);
	}
	
	/**
	 * Represents the model object for the form, which is typically used as a wrapper for the list
	 * of global properties list so that spring can bind the properties of the objects in the list.
	 * Also capable of validating itself
	 */
	public class GlobalPropertiesModel implements Validator {
		
		private List<GlobalProperty> properties;
		
		public GlobalPropertiesModel() {
		}
		
		public GlobalPropertiesModel(List<GlobalProperty> properties) {
			this.properties = properties;
		}
		
		/**
		 * @see org.springframework.validation.Validator#supports(java.lang.Class)
		 */
		@Override
		public boolean supports(Class<?> clazz) {
			return clazz.equals(getClass());
		}
		
		/**
		 * @see org.springframework.validation.Validator#validate(java.lang.Object,
		 *      org.springframework.validation.Errors)
		 */
		@Override
		public void validate(Object target, Errors errors) {
			GlobalPropertiesModel model = (GlobalPropertiesModel) target;
			for (int i = 0; i < model.getProperties().size(); ++i) {
				GlobalProperty gp = model.getProperties().get(i);
				// <editor-fold defaultstate="collapsed" desc="COHORT_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME">
				if (gp.getProperty().equals(MiDoctorPanelConstants.COHORT_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						String cohortIdShowList = model.getProperty(
						    MiDoctorPanelConstants.COHORT_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME).getPropertyValue();
						if (cohortIdShowList.isEmpty()) {
							okay = true;
						} else if (cohortIdShowList.contains(",") && cohortIdShowList.split(",").length > 1) {
							okay = true;
						} else if (!cohortIdShowList.contains(",") && Integer.parseInt(cohortIdShowList) > 0) {
							okay = true;
						}
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".cohortIdShowList.errorMessage");
					}
				}
				// </editor-fold>
				// <editor-fold defaultstate="collapsed" desc="COHORT_MODULE_GLOBAL_PROPERTY_NAME">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.COHORT_MODULE_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						String cohortModuleName = model.getProperty(
						    MiDoctorPanelConstants.COHORT_MODULE_GLOBAL_PROPERTY_NAME).getPropertyValue();
						if (cohortModuleName.equals("reporting") || cohortModuleName.equals("reportingcompatibility"))
							okay = true;
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".cohortModuleName.errorMessage");
					}
				}
				// </editor-fold>
				// <editor-fold defaultstate="collapsed" desc="NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						okay = Integer.valueOf(gp.getPropertyValue()) >= 0;
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".daysWithinRange.errorMessage");
					}
				}
				//</editor-fold>
				// <editor-fold defaultstate="collapsed" desc="NO_OF_DAYS_SEMI_LATE_GLOBAL_PROPERTY_NAME">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.NO_OF_DAYS_SEMI_LATE_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						Integer daysSemiLate = Integer.valueOf(model.getProperty(
						    MiDoctorPanelConstants.NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME).getPropertyValue());
						Integer daysLate = Integer.valueOf(model.getProperty(
						    MiDoctorPanelConstants.NO_OF_DAYS_LATE_GLOBAL_PROPERTY_NAME).getPropertyValue());
						if (Integer.valueOf(gp.getPropertyValue()) > daysSemiLate
						        && Integer.valueOf(gp.getPropertyValue()) <= daysLate) {
							okay = true;
						}
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".daysSemiLate.errorMessage");
					}
				}
				//</editor-fold>
				// <editor-fold defaultstate="collapsed" desc="NO_OF_DAYS_LATE_GLOBAL_PROPERTY_NAME">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.NO_OF_DAYS_LATE_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						Integer daysWithinRange = Integer.valueOf(model.getProperty(
						    MiDoctorPanelConstants.NO_OF_DAYS_WITHIN_RANGE_GLOBAL_PROPERTY_NAME).getPropertyValue());
						Integer daysSemiLate = Integer.valueOf(model.getProperty(
						    MiDoctorPanelConstants.NO_OF_DAYS_SEMI_LATE_GLOBAL_PROPERTY_NAME).getPropertyValue());
						if (Integer.valueOf(gp.getPropertyValue()) > daysWithinRange
						        && Integer.valueOf(gp.getPropertyValue()) > daysSemiLate) {
							okay = true;
						}
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".daysLate.errorMessage");
					}
				}
				//</editor-fold>
				// <editor-fold defaultstate="collapsed" desc="WITHIN_RANGE_COLOR_GLOBAL_PROPERTY_NAME">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.WITHIN_RANGE_COLOR_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						if (gp.getPropertyValue().length() == 6) {
							okay = true;
						}
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".colorWithinRange.errorMessage");
					}
				}
				//</editor-fold>
				// <editor-fold defaultstate="collapsed" desc="SEMI_LATE_COLOR_GLOBAL_PROPERTY_NAME">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.SEMI_LATE_COLOR_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						if (gp.getPropertyValue().length() == 6) {
							okay = true;
						}
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".colorSemiLate.errorMessage");
					}
				}
				//</editor-fold>
				// <editor-fold defaultstate="collapsed" desc="LATE_COLOR_GLOBAL_PROPERTY_NAME">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.LATE_COLOR_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						if (gp.getPropertyValue().length() == 6) {
							okay = true;
						}
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".colorLate.errorMessage");
					}
				}
				//</editor-fold>
				// <editor-fold defaultstate="collapsed" desc="COHORT_ALERT_PORTLET_BGCOLOR_GLOBAL_PROPERTY_NAME">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.COHORT_ALERT_PORTLET_BGCOLOR_GLOBAL_PROPERTY_NAME)) {
					boolean okay = false;
					try {
						if (gp.getPropertyValue().length() == 6) {
							okay = true;
						}
					}
					catch (Exception ex) {}
					if (!okay) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".cohortAlertPortletBgColor.errorMessage");
					}
				}
				//</editor-fold>
				// <editor-fold defaultstate="collapsed" desc="COHORT_ENTRY_DATE_REPORT_QUERY">
				else if (gp.getProperty().equals(MiDoctorPanelConstants.COHORT_ENTRY_DATE_REPORT_QUERY_GLOBAL_PROPERTY_NAME)) {
					boolean valid = false;
					try {
						valid = gp.getPropertyValue().matches("^(\\d+:\\d+;?)+$");
					}
					catch (Exception ex) {}
					if (!valid) {
						errors.rejectValue("properties[" + i + "]", MiDoctorPanelConstants.MODULE_ID
						        + ".entryDateQuery.errorMessage");
					}
					
				}
				//</editor-fold>
			}
		}
		
		/**
		 * Returns the global property for the given propertyName
		 * 
		 * @param propertyName
		 * @return
		 */
		public GlobalProperty getProperty(String propertyName) {
			GlobalProperty prop = null;
			for (GlobalProperty gp : getProperties()) {
				if (gp.getProperty().equals(propertyName)) {
					prop = gp;
					break;
				}
			}
			return prop;
		}
		
		/**
		 * @return
		 */
		public List<GlobalProperty> getProperties() {
			return properties;
		}
		
		/**
		 * @param properties
		 */
		public void setProperties(List<GlobalProperty> properties) {
			this.properties = properties;
		}
	}
}
